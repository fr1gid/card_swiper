<!DOCTYPE HTML>
	<head>
		<meta charset="utf.8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
        <title>BYU - OSC Check In</title>
        <link rel="shortcut icon" href="favicon.ico">
		
        <link href='https://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/" rel="stylesheet" type="text/css"  media="all" />
        <link rel="stylesheet" href="css/byu-header-bar.css">
        <link rel="stylesheet" href="css/base.css">
        <link rel="stylesheet" href="css/responsive.css" media="all and (min-width:16em)">
		<script src="js/modernizr.js"></script>
	</head>
	<body>
		
		<!---start-content---->
        <!-- BYU HEADER BAR-->
        <div id="byu-bar">
            <!-- BYU Logo-->
            <h2><a href="http://www.domainssupport.byu.edu/" class="byu">Brigham Young University</a></h2>
        </div>
        <!-- END BYU HEADER BAR-->    
        <div class="container" align ="center">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Sign in below</h1>
                    <br />
                    <br />
					<?php require 'connect_to_db.php' ?>
                    <?php require 'process_swipe.php' ?>
                    <form method="get">
                        <input id="byu_id_number_collector" type="text" placeholder="Your ID Number" name="id"/>
                        <button type = "submit" class="btn btn-primary">Submit</button>  
                    </form>
                    <p class="text-info">Enter your ID number manually if you forgot your card.</p>
                    <br /><br />
                    <a href="datapage.html">View Reports</a>
                    <br />
                </div> <!-- col -->
            </div> <!-- row -->
		</div> <!-- container -->
	</body>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	
		$(document).ready(function() {
			function setHeight() {
				windowHeight = $(window).innerHeight();
				$('body').css('min-height', windowHeight);
			};
			
			// Set the height of the body to the window height
			setHeight();
			
			// Focus on the main input
			$("#byu_id_number_collector").focus();
			
			// Success alerts are set to clear out after 1 second
			setTimeout(function() {
				$('.alert.alert-success').fadeOut();
			}, 3000);
			
			// Failure alerts are set to clear out after 1 minute
			setTimeout(function() {
				$('.alert.alert-danger').fadeOut();
			}, 60000);
			
			// If the size of the window ever changes, 
			// make sure the body matches it's new height
			$(window).resize(function() {
				setHeight();
			});
		});
	
		
	</script>
	
</html>