<?php 
	/**********************
	* validateInput($input)
	*	This function take in the input submitted by 
	*	the user and outputs a sanitized version of it
	*	with only its numbers.
	***********************/
	function validateInput($input){
		// Get the student ID number that was input, removing anything that shouldn't be there
		$student_number = trim(filter_var($input, FILTER_SANITIZE_NUMBER_INT)); 
		
		// Check for the correct id # length
		if (strlen($student_number) == 14) {
			$student_number = substr($student_number, 0, -5);
		}
		else if (strlen($student_number) != 9 || !is_numeric($student_number)) {
			throw new Exception('The number you entered is not a BYU ID #. Please enter a try again.');
		}
		return $student_number;
	}
	
	/**********************
	* getLastSwipe($connection, $users_id, $current_datetime, $datetime_in_seconds)
	*	This function get the last swipe for a specified user in the database,
	*	parsing the swipe if it exists.
	***********************/
	function getLastSwipe($connection, $users_id, $current_datetime, $datetime_in_seconds) {
		// Get any "swipes" recorded in the swipes table
		$sql = "SELECT * FROM swipes WHERE users_id = " . $users_id . " ORDER BY id DESC LIMIT 1"; // Only get the last row
		if (!$result = $connection->query($sql)) {
			throw new Exception('There was an error while trying to collect your past data. Please try again.');
		}
		
		if ($result->num_rows > 0) { // Check the users last swipe, if there is one
			$row = $result->fetch_assoc();
			
			$swipe['swipes_id'] = $row["id"];
			$swipe['swipes_users_id'] = $row["users_id"];
			
			// If an "in" time is listed
			if (!empty($row["in_time"])) {
				$swipe['swipes_in_time'] = date('Y-m-d H:i:s', strtotime($row["in_time"]));
			}
			else {
				// There should always be an "in" swipe in any previous rows
				throw new Exception('There was an error in the last swipe that you logged. Please try again.');
			}
			
			if (!empty($row["out_time"])) {
				$swipe['swipes_out_time'] = date('Y-m-d H:i:s', strtotime($row["out_time"]));
			}
			else {
				// Check if the "in" swipe was within the last 6 hours (comparison is done in seconds)
				$swipe['within_hours'] = ($datetime_in_seconds - strtotime($swipe['swipes_in_time'])) < 21600; // 21600 is 6 hours in seconds
				$swipe['swipes_out_time'] = NULL;
			}
			
			$swipe['swipes_status'] = $row["status"];
			return $swipe;
		}
		else {
			return NULL;
		}
	}
	
	/**********************
	* clockIn($connection, $current_datetime, $id)
	*	This function clocks a user in by inserting a new row 
	*	in the database with a datetime as the time_in.
	***********************/
	function clockIn($connection, $current_datetime, $id) {
		// Note: The database is set to EST, so the current "$current_datetime" is used instead on NOW()
		$sql = "INSERT INTO swipes (users_id, in_time, status) VALUES (" . $id . ", '" . $current_datetime . "', \"working\")";
		if (!$result = $connection->query($sql)) {
			throw new Exception('There was an error while trying to check in. Please try again!');
		}
	}
	
	/**********************
	* clockOut($connection, $current_datetime, $id)
	*	This function clocks a user out by updating the last
	*	swipes row and inserting a time_out datetime.
	***********************/
	function clockOut($connection, $current_datetime, $id) {
		$sql = "UPDATE swipes SET out_time = '" . $current_datetime . "', status = 'off' WHERE id = " . $id;
		if (!$result = $connection->query($sql)) {
			throw new Exception('There was an error while trying to check out. Please try again!');
			return;
		}
	}
	
	/**********************
	* processSwipe()
	*	This function processes a swipe by collected a BYU ID #'s
	*	associated student data and logging the student "in" or
	*	"out" accordingly.
	***********************/
	function processSwipe() {
		
		try {
			// Check for HTTP GET variables
			if (isset($_GET['id'])) {
				
				$student_number = validateInput($_GET['id']);
				$connection = connectToDB();
				
				$sql = "SELECT * FROM users WHERE id_number = " . intval($student_number);
				if (!$result = $connection->query($sql)) {
					throw new Exception('There was an error while trying to look up the user. Please try again.');
				}
				
				if ($result->num_rows > 0) { // If user is already in database
					// Only get the first row (there should only be one user entry)
					$row = $result->fetch_array();
					
					$users_id = $row["id"];
					$users_netid = $row["netid"];
					$users_full_name = $row["full_name"];
					
					$name = $users_full_name;
				} 
				else { // If user is not already in database
					
					// TODO: get netid and full name from PI API using the BYU ID #, logging it into the "users" table.
					
					$name = NULL; // Make this the name of the user; it prints out later...
					
					// TODO: Remove below 2 lines once API is implemented
					echo "No user with the id number '" . $student_number . "' was returned from the database.<br>";
					return;
				}
				
				// Get the current datetime
				$current_datetime = date("Y-m-d H:i:s");
				$datetime_in_seconds = strtotime($current_datetime);
				
				try {
					$swipe = getLastSwipe($connection, $users_id, $current_datetime, $datetime_in_seconds);
				}
				catch (Exception $e) {
					$swipe = NULL; // There was no last swipe, set to NULL
				}
				
				// Clocking in (new row)
				if (empty($swipe) || // No rows yet or error collecting last row
					(!empty($swipe['swipes_in_time']) && !empty($swipe['swipes_out_time'])) || // Full row
					(!empty($swipe['swipes_in_time']) && empty($swipe['swipes_out_time']) && !$swipe['within_hours'])) { // Not within given amount of time (currently 6 hours)
					
					clockIn($connection, $current_datetime, $users_id);
				}
				// Clocking out
				else if ($swipe['within_hours']) {
					clockOut($connection, $current_datetime, $swipe['swipes_id']);
				}
				// Else: Error
				else {
					throw new Exception('There was an unexpected and very ambiguous error. Please try again!');
				}
				
				// If the name is known
				if (!empty($name)) {
					$success_text = 'Welcome ' . $name . '!<br>Your BYU ID number has been recorded!<br>';
				}
				else { // The name is empty for some reason, still a success, right?
					$success_text = 'Welcome!<br>Your BYU ID number has been recorded!<br>';
				}
				$success_div = '<div class="alert alert-success">' . $success_text . '</div>';
				echo $success_div;
				
				// Close the connection with the database
				$connection->close();
			}
		} catch (Exception $e) {
			echo '<div class="alert alert-danger">' . $e->getMessage() . "<br>If problems persist, please contact David Owens or Jose Montoya.<br></div>";
		}
	}
	
	processSwipe();
?>