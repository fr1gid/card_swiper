<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js oldie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js oldie lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js oldie lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
	<!-- <![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>BYU - OSC Check In</title>
		<meta name="viewport" content="width=device-width, , initial-scale=1">
		<!-- Place favicon.ico and apple-touch-icon.png in the root directory-->
		<link rel="shortcut icon" href="favicon.ico">
		<!-- Incorporate these styles into your stylesheet.-->
		<link rel="stylesheet" href="css/byu-header-bar.css">
		<link rel="stylesheet" href="css/base.css">
		<link rel="stylesheet" href="css/responsive.css" media="all and (min-width:16em)">
		<!-- Modernizr not required, but SVG support helpful.-->
		<script src="js/modernizr.js"></script>
	</head>
	<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an<strong>outdated</strong>browser. Please<a href="http://browsehappy.com/">upgrade your browser</a>or<a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a>to improve your experience.</p>
    <![endif]-->
    <!-- BYU HEADER BAR-->
    <div id="byu-bar">
        <!-- BYU Logo-->
        <h2><a href="http://www.domainssupport.byu.edu/" class="byu">Brigham Young University</a></h2>
    </div>
    <!-- END BYU HEADER BAR-->
    <div class="centered">
	<a href="index.php">Return to Check In Page</a>
	</div>
	</br>
    <div class="container">
		<div class="table-striped">
			<table class="table">
				<thead>
					 <tr>
						<th>Name</th>
						<th>NetID</th>
						<th>Time In</th>
						<th>Time Out</th>
					</tr>
				</thead>
				<tbody>
					 <tr>
						<td>John</td>
						<td>Doe</td>
						<td>john@example.com</td>
						<td>Doe</td>
					</tr>
					<tr>
						<td>Mary</td>
						<td>Moe</td>
						<td>mary@example.com</td>
						<td>Doe</td>
					</tr>
					<tr>
						<td>July</td>
						<td>Dooley</td>
						<td>july@example.com</td>
						<td>Doe</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	</body>
</html>



